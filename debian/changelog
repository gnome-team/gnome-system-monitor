gnome-system-monitor (48~rc-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 06 Mar 2025 14:08:54 -0500

gnome-system-monitor (48~beta-1) unstable; urgency=medium

  * New upstream release
  * Add Build-Depends: libcatch2-dev

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 05 Feb 2025 16:59:47 -0500

gnome-system-monitor (47.0-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 16 Sep 2024 08:06:02 -0400

gnome-system-monitor (47~rc-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Thu, 05 Sep 2024 14:53:56 +0200

gnome-system-monitor (47~beta-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum libadwaita
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 09 Aug 2024 14:55:09 -0400

gnome-system-monitor (46.0-1) unstable; urgency=medium

  * New upstream release
  * Update Homepage

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Mar 2024 16:46:49 -0400

gnome-system-monitor (46~beta-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 12 Feb 2024 15:24:12 -0500

gnome-system-monitor (46~alpha-1) unstable; urgency=medium

  * New upstream release
  * Build-Depend on gtkmm4 & libadwaita instead of gtkmm3
  * debian/control: Bump minimum libgtop to 2.41.2
  * Stop using debian/control.in and dh_gnome_clean

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 11 Jan 2024 11:20:35 -0500

gnome-system-monitor (45.0.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Jarred Wilson <jarred.wilson@canonical.com>  Mon, 18 Sep 2023 12:44:20 -0400

gnome-system-monitor (45.0.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Jarred Wilson <jarred.wilson@canonical.com>  Wed, 06 Sep 2023 15:48:18 -0400

gnome-system-monitor (45.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 16 Aug 2023 14:42:05 -0400

gnome-system-monitor (45~beta-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 10 Aug 2023 14:47:44 -0400

gnome-system-monitor (44.0-2) unstable; urgency=medium

  * Update standards version to 4.6.2, no changes needed
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 23 Jul 2023 16:20:17 -0400

gnome-system-monitor (44.0-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Amin Bandali <bandali@ubuntu.com>  Mon, 27 Mar 2023 11:56:02 -0400

gnome-system-monitor (44~beta-1) experimental; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Drop all patches: applied in new release

  [ Debian Janitor ]
  * Add debian/upstream/metadata
  * Set field Upstream-Name in debian/copyright.

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 13 Feb 2023 14:08:12 -0500

gnome-system-monitor (42.0-2) unstable; urgency=medium

  * Team upload
  * d/control.in: Build-Depend on polkitd instead of transitional policykit-1
  * d/control.in: Add Suggests on pkexec.
    This is used to elevate privileges to root if you ask to terminate a
    process belonging to another user.
  * d/patches: Update translations from upstream gnome-42 branch
  * Standards-Version: 4.6.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Fri, 14 Oct 2022 10:13:57 +0100

gnome-system-monitor (42.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 21 Mar 2022 10:13:27 -0400

gnome-system-monitor (42~rc-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum meson to 0.57.0

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Wed, 09 Mar 2022 08:21:52 -0500

gnome-system-monitor (42~alpha-1) unstable; urgency=medium

  * New upstream release
  * Drop patch: applied in new release

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Fri, 18 Feb 2022 13:26:52 -0500

gnome-system-monitor (41.0-2) unstable; urgency=medium

  * Cherry-pick patch to support GNOME 42 dark theme preference
  * debian/control.in: Bump minimum libhandy to 1.5.90

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Wed, 16 Feb 2022 14:57:27 -0500

gnome-system-monitor (41.0-1) unstable; urgency=medium

  * New upstream release
  * debian/rules: Simplify a bit
  * Bump Standards-Version to 4.6.0
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 21 Sep 2021 20:19:43 -0400

gnome-system-monitor (41~rc-1) experimental; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 10 Sep 2021 11:41:25 +0200

gnome-system-monitor (40.1-2) unstable; urgency=medium

  * debian/rules: Drop unneeded -Wl,--as-needed
  * Bump debhelper-compat to 13
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 20 Aug 2021 07:40:34 -0400

gnome-system-monitor (40.1-1) experimental; urgency=medium

  * New upstream release
  * debian/patches/git_nongnome_segfault.patch:
    - remove patch included in the new version
  * debian/watch:
    - Update to follow the new GNOME version scheme

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 03 May 2021 13:10:09 +0200

gnome-system-monitor (40.0-2) experimental; urgency=medium

  * debian/patches/git_nongnome_segfault.patch:
    - Dropped non-gnome workaround causing crash (lp: #1921583)

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 30 Mar 2021 11:10:35 +0200

gnome-system-monitor (40.0-1) experimental; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 22 Mar 2021 21:15:25 +0100

gnome-system-monitor (40~rc-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in:
    - Build with libhandy-1-dev

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 15 Mar 2021 11:10:19 +0100

gnome-system-monitor (40~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in:
    - build-depends on the libatk bindings

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 24 Feb 2021 10:09:03 +0100

gnome-system-monitor (3.38.0-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 15 Sep 2020 16:05:26 +0200

gnome-system-monitor (3.37.90-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in:
    - updated meson requirement

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 07 Aug 2020 17:18:10 +0200

gnome-system-monitor (3.36.0-1) unstable; urgency=medium

  * New upstream release
  * debian/patches/git_polkit_requirement.patch:
    - removed, the change is included in the new version

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 10 Mar 2020 12:03:58 +0100

gnome-system-monitor (3.35.90-1) experimental; urgency=medium

  * New upstream release
  * debian/patches/git_polkit_requirement.patch:
    - backport git commits to avoid a depends on a newer polkit

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 06 Feb 2020 10:06:20 +0100

gnome-system-monitor (3.32.1-2) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 19 Sep 2019 20:29:15 -0400

gnome-system-monitor (3.32.1-1) experimental; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 09 Apr 2019 10:23:59 +0200

gnome-system-monitor (3.32.0-1) experimental; urgency=medium

  * New upstream release
    + fixed issue with system icon change

 -- Iain Lane <laney@debian.org>  Mon, 11 Mar 2019 17:20:09 +0000

gnome-system-monitor (3.31.90-1) experimental; urgency=medium

  * New upstream development release
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 07 Feb 2019 19:58:30 -0500

gnome-system-monitor (3.30.0-2) unstable; urgency=medium

  * Restore -Wl,-O1 to our LDFLAGS
  * Revert "debian/watch: Watch for unstable releases"

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 14 Dec 2018 11:28:24 -0500

gnome-system-monitor (3.30.0-1) unstable; urgency=medium

  * New upstream version

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 04 Sep 2018 12:02:19 +0200

gnome-system-monitor (3.29.91-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * Update GLib build-dependency
  * d/copyright: Update and convert to machine-readable format
  * Build using Meson
  * Set Rules-Requires-Root to no
  * Don't install the detailed upstream changelog, only the NEWS file
  * Standards-Version: 4.2.0 (no further changes)

 -- Simon McVittie <smcv@debian.org>  Tue, 14 Aug 2018 08:32:06 +0100

gnome-system-monitor (3.28.2-1) unstable; urgency=medium

  * New upstream release (LP: #1770971)

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 13 May 2018 11:20:50 -0400

gnome-system-monitor (3.28.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.1.4

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 11 Apr 2018 13:13:46 -0400

gnome-system-monitor (3.28.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 17 Mar 2018 10:08:50 -0400

gnome-system-monitor (3.27.92-1) unstable; urgency=medium

  * New upstream release candidate
  * Release to unstable

 -- Tim Lunn <tim@feathertop.org>  Wed, 07 Mar 2018 20:44:37 +1100

gnome-system-monitor (3.27.90-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in:
    - Bump minimum libglib2.0-dev to 2.55.0
    - Build-Depend on policykit-1
    - Drop obsolete intltool Build-Depends
  * Drop git-fix-lp1723370.patch: Applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 09 Feb 2018 15:22:31 -0500

gnome-system-monitor (3.26.0-3) unstable; urgency=medium

  [ Daniel van Vugt ]
  * Add debian/patches/git-fix-lp1723370.patch:
    backport commit to fix inaccurate %CPU values in the Processes table
    (LP: #1723370)

  [ Jeremy Bicha ]
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 21 Jan 2018 16:21:26 -0500

gnome-system-monitor (3.26.0-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 15 Dec 2017 17:37:50 -0500

gnome-system-monitor (3.26.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 11 Sep 2017 20:22:42 -0400

gnome-system-monitor (3.25.91-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - Bump minimum libglib2.0-dev to >= 2.46.0, libgtk-3-dev to >= 3.22.0,
      libglibmm-2.4-dev to >= 2.46.0, and libgtop2-dev to >= 2.37.2
  * Drop unsupported --enable-wnck option (LP: #1665209)
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 03 Sep 2017 12:38:21 -0400

gnome-system-monitor (3.22.2-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Tue, 08 Nov 2016 01:42:45 +0100

gnome-system-monitor (3.22.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 19 Sep 2016 20:51:58 +0200

gnome-system-monitor (3.21.92-1) unstable; urgency=medium

  * New upstream development release.
  * Bump Standards-Version to 3.9.8.

 -- Michael Biebl <biebl@debian.org>  Tue, 13 Sep 2016 01:50:20 +0200

gnome-system-monitor (3.21.91-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Convert from cdbs to dh
  * Add debian/manpages and debian/docs
  * Bump dh compat to 10
  * Enable hardening flags
  * Drop debian/source/lintian-overrides:
    - Issue was fixed a few years ago

  [ Pedro Beja ]
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 30 Aug 2016 17:11:36 +0200

gnome-system-monitor (3.20.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Fri, 15 Apr 2016 18:18:45 +0200

gnome-system-monitor (3.20.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 24 Mar 2016 16:03:40 +0100

gnome-system-monitor (3.19.92-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.7

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 14 Mar 2016 21:24:16 +0100

gnome-system-monitor (3.18.2-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Wed, 11 Nov 2015 15:49:00 +0100

gnome-system-monitor (3.18.0.1-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 03 Oct 2015 11:21:41 +0200

gnome-system-monitor (3.18.0-1) unstable; urgency=medium

  [ Michael Biebl ]
  * Remove Debian menu entry.

  [ Andreas Henriksson ]
  * New upstream release.
  * Drop debian/patches/0001-Add-support-for-libsystemd.patch
    - now included in upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 22 Sep 2015 15:05:31 +0200

gnome-system-monitor (3.16.0-2) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Switch build-dependency from appdata-tools to appstream-util

  [ Michael Biebl ]
  * Bump debhelper compatibility level to 9.
  * Add Homepage: field.

 -- Michael Biebl <biebl@debian.org>  Tue, 01 Sep 2015 11:19:34 +0200

gnome-system-monitor (3.16.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop obsolete --disable-scrollkeeper configure switch.
  * Build against libsystemd. (Closes: #779753)

 -- Michael Biebl <biebl@debian.org>  Tue, 19 May 2015 00:06:46 +0200

gnome-system-monitor (3.14.1-1) unstable; urgency=medium

  [ Jackson Doak ]
  * debian/control.in: Add build-depends on appdata-tools, desktop-file-utils
    - Allows build time file validation

  [ Laurent Bigonville ]
  * New upstream release.
  * debian/control.in: Bump Standards-Version to 3.9.6 (no further changes)
  * Add debian/source/lintian-overrides: Add an override as some files are not
    properly cleaned up when the tarball is built.

 -- Laurent Bigonville <bigon@debian.org>  Sat, 18 Oct 2014 10:40:12 +0200

gnome-system-monitor (3.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 22 Sep 2014 19:50:52 +0200

gnome-system-monitor (3.13.91-1) experimental; urgency=medium

  * New upstream development release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 05 Sep 2014 15:21:14 -0700

gnome-system-monitor (3.13.90-1) experimental; urgency=medium

  * New upstream development release.
  * Update build-dependencies according to configure.ac:
    - drop gnome-icon-theme
    - bump libgtk-3-dev to (>= 3.12.0)

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 28 Aug 2014 23:19:57 -0700

gnome-system-monitor (3.12.2-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 16 May 2014 11:51:32 +0200

gnome-system-monitor (3.12.1-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 09 May 2014 19:57:25 +0200

gnome-system-monitor (3.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop debian/patches/01-fix_ftbfs_hurd.patch, merged upstream.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 24 Mar 2014 22:33:35 +0100

gnome-system-monitor (3.11.90-1) experimental; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 17 Feb 2014 22:12:59 +0100

gnome-system-monitor (3.10.2-2) unstable; urgency=medium

  * Upload to unstable
  * debian/patches/01-fix_ftbfs_hurd.patch: Fix FTBFS on hurd-i386
    (Closes: #738280)
  * Only enable systemd-logind support on linux architectures

 -- Laurent Bigonville <bigon@debian.org>  Sun, 16 Feb 2014 13:05:55 +0100

gnome-system-monitor (3.10.2-1) experimental; urgency=low

  * New upstream release.
    - Drop debian/patches/01-kfreebsd-define.patch: Applied upstream
    - debian/control.in: Bump build-dependencies
  * debian/control.in:
    - Bump Standards-Version to 3.9.5 (no further changes)
    - Add dependency against libsystemd-login-dev (>= 44)
  * debian/rules: Explicitly pass --enable-wnck and --enable-systemd to the
    configure

 -- Laurent Bigonville <bigon@debian.org>  Sat, 14 Dec 2013 16:17:43 +0100

gnome-system-monitor (3.8.2.1-2) unstable; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in, debian/rules:
    - Run autoreconf
    - Don't run autopoint, it causes configure error with intltool

  [ Emilio Pozuelo Monfort ]
  * debian/patches/01-kfreebsd-define.patch:
    + New patch from Petr Salinger to fix the kfreebsd build.
      Closes: #712703.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Tue, 18 Jun 2013 22:01:52 +0200

gnome-system-monitor (3.8.2.1-1) unstable; urgency=low

  * New upstream release.
  * Upload to unstable.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 25 May 2013 10:31:33 +0200

gnome-system-monitor (3.8.0-1) experimental; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in:
    - Drop build-depends on itstool and rarian-compat

  [ Andreas Henriksson ]
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 04 Apr 2013 14:33:12 +0200

gnome-system-monitor (3.7.92-1) experimental; urgency=low

  * New upstream release.
  * Bump build-dependencies according to configure.ac:
    - libgtk-3-dev bumped from (>= 3.0.0) to (>= 3.5.12)
    - libgtkmm-3.0-dev bumped from (>= 2.99) to (>= 3.3.18)
  * Change build-dependencies for new documentation infrastructure:
   - drop gnome-doc-tools
   - add yelp-tools and itstool
  * Drop patches
    - 03_dont_show_in_KDE.patch and 04_kfreebsd_cputime.patch both
      applied upstream.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 22 Mar 2013 16:44:14 +0100

gnome-system-monitor (3.4.1-2) unstable; urgency=low

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Thu, 19 Apr 2012 03:36:11 +0200

gnome-system-monitor (3.4.1-1) experimental; urgency=low

  * New upstream release.
  * debian/patches/04_kfreebsd_cputime.patch: Fix CPU time accounting for
    (k)freebsd. Original patch by Christoph Egger, thanks!
    Closes: #665999

 -- Michael Biebl <biebl@debian.org>  Tue, 17 Apr 2012 18:47:53 +0200

gnome-system-monitor (3.4.0-1) experimental; urgency=low

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Tue, 27 Mar 2012 17:01:46 +0200

gnome-system-monitor (3.3.92-1) experimental; urgency=low

  * New upstream development release.
  * debian/patches/03_dont_show_in_KDE.patch: Refreshed.

 -- Michael Biebl <biebl@debian.org>  Tue, 20 Mar 2012 02:12:29 +0100

gnome-system-monitor (3.3.91-1) experimental; urgency=low

  [ Jeremy Bicha ]
  * New upstream release.
  * debian/control.in:
    - Standards-Version 3.9.3
    - Bump librsvg-dev to (>= 2.35.0)
  * debian/patches/03_dont_show_in_KDE.patch:
    - Buzilla patch to not show in KDE's menus since they have their
      own System Monitor. Closes: #650019

 -- Michael Biebl <biebl@debian.org>  Sat, 17 Mar 2012 20:52:14 +0100

gnome-system-monitor (3.2.1-1) unstable; urgency=low

  * New upstream release.
  * debian/watch:
    - Track .xz tarballs.
  * debian/control.in:
    - Set pkg-gnome-maintainers@lists.alioth.debian.org as Maintainer.

 -- Michael Biebl <biebl@debian.org>  Wed, 19 Oct 2011 01:14:11 +0200

gnome-system-monitor (3.2.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Thu, 29 Sep 2011 12:52:29 +0200

gnome-system-monitor (3.0.1-1) unstable; urgency=low

  [ Jean Schurger ]
  * New upstream release.
  * debian/control.in
    - Update build-dependencies, especially for Gnome 3
    - Updated Standards-Version to 3.9.2
    - Removed dependency on libdbus-glib-1-dev
  * Added debian/source/format

  [ Michael Biebl ]
  * debian/control.in
    - Remove Build-Depends on quilt.
    - Add Vcs-* fields.
  * debian/rules
    - Remove patchsys-quilt.mk include.
  * debian/watch
    - Switch to .bz2 tarballs.
  * Bump debhelper compatibility level to 8.
  * Drop debian/patches/01_load_library_instead_of_so.patch and the Recommends
    on libgksu2-0. The current gksu integration is broken and should be
    replaced by PolicyKit.

 -- Michael Biebl <biebl@debian.org>  Tue, 21 Jun 2011 16:49:23 +0200

gnome-system-monitor (2.28.1-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 12 Apr 2010 10:13:22 +0200

gnome-system-monitor (2.28.0-1) unstable; urgency=low

  * New upstream release.
    - Bump libgtk2.0-dev minimun build dependency.
  * Standards-Version is 3.8.3, no changes needed.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Mon, 21 Sep 2009 23:35:21 +0200

gnome-system-monitor (2.26.2-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Stop mentioning GNOME 2.
  * Remove scrollkeeper dependency.

  [ Luca Bruno ]
  * New upstream release.
  * debian/control.in:
    - Updated Standards-Version to 3.8.1, no additional changes needed.
  * debian/copyright:
    - Specify GPL version 2.

  [ Emilio Pozuelo Monfort ]
  * New upstream bugfix release.
  * debian/patches/80_use_units_coherent_with_gnome_desktop.patch:
    removed, it's not needed anymore as GVFS fixes it, and wasn't
    applied anyway.

  [ Josselin Mouette ]
  * Minor build-dependencies updates.

 -- Josselin Mouette <joss@debian.org>  Sat, 27 Jun 2009 00:39:45 +0200

gnome-system-monitor (2.24.4-1) unstable; urgency=low

  * New upstream release.

 -- Josselin Mouette <joss@debian.org>  Wed, 11 Mar 2009 22:44:31 +0100

gnome-system-monitor (2.24.1-1) experimental; urgency=low

  [ Emilio Pozuelo Monfort ]
  * New upstream release:
    - Fixed performance issues with some graphic drivers. Closes: #471152.
  * Remove 'debian uupdate' from watch file.
  * Fix menu section.

  [ Josselin Mouette ]
  * Recommend gvfs so that help actually shows up.

 -- Josselin Mouette <joss@debian.org>  Tue, 09 Dec 2008 09:38:15 +0100

gnome-system-monitor (2.22.3-1) unstable; urgency=low

  * New upstream bugfix release.
  * debian/control.in:
    + Updated Standards-Version to 3.8.0, no additional changes needed.
    + Build depend on intltool as it's needed now.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 01 Jul 2008 10:40:58 +0200

gnome-system-monitor (2.22.2-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 02 Jun 2008 12:39:09 +0200

gnome-system-monitor (2.22.1-1) unstable; urgency=low

  * New upstream bugfix release.
  * debian/patches/series:
    + 80_use_units_coherent_with_gnome_desktop.patch:
      - Disable this one as gvfs uses kibibytes, etc too now.
  * debian/rules:
    + Link with -Wl,-z,defs -Wl,-O1 -Wl,--as-needed to get rid of some
      dependencies.
    + Clean up configure parameters.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 14 Apr 2008 19:24:34 +0200

gnome-system-monitor (2.22.0-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Drop 04_menu_change.patch, this application has nothing to do in the
    settings menu. Closes: #467396.

  [ Sebastian Dröge ]
  * New upstream stable release:
    + debian/control.in:
      - Update build dependencies.
    + debian/patches/80_use_units_coherent_with_gnome_desktop.patch:
      - Updated to apply cleanly again and define a function with the
        correct signature instead of defining to one with another signature.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 14 Mar 2008 12:04:47 +0100

gnome-system-monitor (2.20.2-1) unstable; urgency=low

  * New upstream bugfix release:
    + debian/patches/02_fix-distclean.patch:
      - Dropped, merged upstream.
  * debian/control.in:
    + Update Standards-Version to 3.7.3, no additional changes needed.
  * debian/menu:
    + Fix section.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 08 Jan 2008 20:23:56 +0100

gnome-system-monitor (2.20.1-1) unstable; urgency=low

  [ Josselin Mouette ]
  * 02_fix-distclean.patch: don't remove gnome-doc-utils.make in the
    distclean target. Closes: #442578.
  * Switch to quilt for patch handling to work around #414305.
    Build-depend on quilt.
  * Also include patchsys-quilt.mk after gnome.mk.

  [ Sebastian Dröge ]
  * New upstream bugfix release.

 -- Sebastian Dröge <slomo@debian.org>  Thu, 25 Oct 2007 13:57:05 +0200

gnome-system-monitor (2.20.0-1) unstable; urgency=low

  * New upstream release:
    + Fixes invalid conversion from 'pid_t*' to 'unsigned int'
      (Closes: #443007).
    + debian/control.in:
      - Update libgtop2-dev build dependency to >= 2.19.3.
      - Build depend on libpcre3-dev >= 6.4 and libgtkmm-2.4-dev >= 2.8.
    + debian/rules:
      - Enable pcre support.
  * debian/patches/80_use_units_coherent_with_gnome_desktop.patch:
    + Use units coherent with the other Gnome apps, i.e. don't use
      kibibytes, mebibytes and friends. Patch by Christian Persch, taken
      from Ubuntu.

 -- Sebastian Dröge <slomo@debian.org>  Sat, 29 Sep 2007 06:17:12 +0200

gnome-system-monitor (2.18.2-1) unstable; urgency=low

  * New upstream stable release.
  * Wrap build-deps and deps.
  * Fix URL in copyright.
  * Cleanups.
  * Drop check-dist include...

 -- Loic Minier <lool@dooz.org>  Mon, 28 May 2007 16:57:02 +0200

gnome-system-monitor (2.18.1.1-1) unstable; urgency=low

  [ Riccardo Setti ]
  * New upstream release.
  * Added patch 01_load_library_instead_of_so.patch,
    04_menu_change.patch, taken from the Ubuntu package.
  * Dropped libgnome2-dev, libgnomeui-dev build-depends
  * Added gnome-doc-utils build-deps
  * Bumped debhelper, libgtop build-deb

  [ Loic Minier ]
  * Add a get-orig-source target to retrieve the upstream tarball.
  * Include the new check-dist Makefile to prevent accidental uploads to
    unstable; bump build-dep on gnome-pkg-tools to >= 0.10.

 -- Riccardo Setti <giskard@debian.org>  Wed, 16 May 2007 16:30:03 +0200

gnome-system-monitor (2.16.1-1) experimental; urgency=low

  * New upstream release
    - Remove libgksu1.2-dev build-dep. [debian/control.in]
    - Remove libgksuui1.0-dev build-dep. [debian/control.in]
    - Add pkg-config build-dep. [debian/control.in]
    - Add gnome-icon-theme build-dep. [debian/control.in]
    - Remove --enable-gksu configure flag. [debian/rules]
    - Require libgtop2-dev >= 2.14.4 to match configure.in.
      [debian/control.in]

 -- Oystein Gisnas <oystein@gisnas.net>  Wed,  4 Oct 2006 23:36:17 +0200

gnome-system-monitor (2.14.5-2) UNRELEASED; urgency=low

  * Fix bashism.

 -- Loic Minier <lool@dooz.org>  Sun,  3 Sep 2006 12:16:51 +0200

gnome-system-monitor (2.14.5-1) unstable; urgency=low

  * New upstream release.
    - Drop 70_autoconf-2.59a-9 patch.

 -- Loic Minier <lool@dooz.org>  Fri, 14 Jul 2006 11:03:08 +0200

gnome-system-monitor (2.14.4-1) unstable; urgency=low

  [ Oystein Gisnas ]
  * New upstream release
    - Bump libgtk2.0-dev build-dep to >= 2.8.0. [debian/control.in]
    - Bump libgtop2-dev build-dep to >= 2.14.1-1. [debian/control.in]
    - Add build-deps libgksu1.2-dev, libgksuui1.0-dev. [debian/control.in]
  * Update watch file to look in even-numbered dirs. [debian/watch]
  * Bump Standards-Version to 3.7.2. [debian/control.in]
  * Regenerate debian/control

  [ Loic Minier ]
  * Update list of authors.
  * Add a libglib2.0-dev (>= 2.9.1) build-dep.
  * Bump libgksuui1.0-dev build-dep to >= 1.0.0, libgksu1.2-dev to >= 0.15.0.
  * Downgrade libgtop2-dev to >= 2.13.0.
  * New patch, 70_autoconf-2.59a-9, to fix the configure script of the
    upstream tarball as it was generated with a recent configure exposing a
    bug in the AM_GLIB_DEFINE_LOCALEDIR macro; I used version 2.59a-9 of
    autoconf to produce the patch.

 -- Loic Minier <lool@dooz.org>  Sat,  8 Jul 2006 19:48:19 +0200

gnome-system-monitor (2.12.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Jordi Mallach <jordi@debian.org>  Mon,  9 Jan 2006 18:03:34 +0100

gnome-system-monitor (2.12.2-1) experimental; urgency=low

  * New upstream release.
  * debian/control.in: get rid of references to the GNOME1 package name.
    We're at the gates of 2006...

 -- Jordi Mallach <jordi@debian.org>  Sun, 25 Dec 2005 23:52:42 +0100

gnome-system-monitor (2.12.1-2) experimental; urgency=low

  * Create patches directory. [debian/patches]
  * Build with fixed libgtop2 shlibs and build-depend on >= 2.12.0-2.
    (Closes: #336760) [debian/control, debian/control.in]

 -- Loic Minier <lool@dooz.org>  Tue,  1 Nov 2005 12:41:42 +0100

gnome-system-monitor (2.12.1-1) experimental; urgency=low

  * New upstream releases.
    - Fixes non-breaking spaces in German translation. (Closes: #313055)
    - Update watch file. [debian/watch]
    - Bump libgtk2.0-dev build-dep to >= 2.6.0.
      [debian/control, debian/control.in]
    - Add build-dep on libgnomevfs2-dev >= 2.6.0.
      [debian/control, debian/control.in]
  * Fix license, doh! [debian/copyright]
  * Don't overwrite DEB_INSTALL_MANPAGES_*. [debian/rules]
  * Remove obsolete Conflicts/Replaces. [debian/control, debian/control.in]
  * Manual page is in section 1. [debian/gnome-system-monitor.sgml]

 -- Loic Minier <lool@dooz.org>  Sat, 29 Oct 2005 21:33:23 +0200

gnome-system-monitor (2.10.1-3) unstable; urgency=high

  [ Josselin Mouette ]
  * Standards-version is 3.6.2.
  * Depend on ${misc:Depends} (high urgency fix).

  [ Loic Minier ]
  * Add CDBS' utils.

 -- Loic Minier <lool@dooz.org>  Fri, 14 Oct 2005 11:28:32 +0200

gnome-system-monitor (2.10.1-2) unstable; urgency=low

  * Upload to unstable.
    - asks for the root passwd when renicing (closes: #187099).
    - can reorder columns (closes: #228650).
    - fixes a crash on amd64 (closes: #304999).

 -- Jordi Mallach <jordi@debian.org>  Sun, 12 Jun 2005 18:43:24 +0200

gnome-system-monitor (2.10.1-1) experimental; urgency=low

  * New upstream version.
  * debian/rules:
    - clean the scrollkeeper files.
  * debian/watch:
    - updated.

 -- Sebastien Bacher <seb128@debian.org>  Thu, 14 Apr 2005 14:12:40 +0200

gnome-system-monitor (2.8.1-5) testing-proposed-updates; urgency=medium

  * Testing-proposed updates upload targetted at etch to allow 2.8.1-4 into
    sarge.

 -- Loic Minier <lool@dooz.org>  Wed, 31 Aug 2005 21:16:24 +0200

gnome-system-monitor (2.8.1-4) stable; urgency=medium

  * Stable upload targetted at Sarge.
  * Add a trivial patch from upstream fixing crashes when special filesystem
    types are used on a system. [debian/patches/50_special-fs-segfault.patch]
    (Closes: #289384, #311714)

 -- Loic Minier <lool@dooz.org>  Fri, 26 Aug 2005 18:38:24 +0200

gnome-system-monitor (2.8.1-2) unstable; urgency=low

  * debian/rules:
    - don't install scrollkeeper files.

 -- Sebastien Bacher <seb128@debian.org>  Wed,  1 Jun 2005 15:55:56 +0200

gnome-system-monitor (2.8.1-1) unstable; urgency=low

  * GNOME team upload.
  * New upstream release.

 -- Jordi Mallach <jordi@debian.org>  Thu, 23 Dec 2004 14:14:31 +0100

gnome-system-monitor (2.8.0-1) unstable; urgency=low

  * New upstrem release.
  * debian/menu:
    - added missing quotes.
  * debian/patches/src_omf_make.patch:
    - not needed with the new version.
  * debian/watch:
    - added.

 -- Sebastien Bacher <seb128@debian.org>  Sun, 28 Nov 2004 00:40:44 +0100

gnome-system-monitor (2.6.0-5) unstable; urgency=low

  * debian/gnome-system-monitor.postinst/postrm:
    - removed, dh_gconf and dh_scrollkeeper handle that.

 -- Sebastien Bacher <seb128@debian.org>  Fri,  6 Aug 2004 20:57:07 +0000

gnome-system-monitor (2.6.0-4) unstable; urgency=low

  * debian/control.in:
    + updated Build-Depends libgtop2-dev (Closes: #251710).

 -- Sebastien Bacher <seb128@debian.org>  Sun, 30 May 2004 18:12:28 +0000

gnome-system-monitor (2.6.0-3) unstable; urgency=low

  * Rebuilt for unstable with libgtop2-2.

 -- Sebastien Bacher <seb128@debian.org>  Fri, 28 May 2004 23:23:20 +0200

gnome-system-monitor (2.6.0-2) experimental; urgency=low

  * Added missing Build-Depends on libxml-parser-perl (Closes: #240748).

 -- Sebastien Bacher <seb128@debian.org>  Sat,  3 Apr 2004 22:24:44 +0200

gnome-system-monitor (2.6.0-1) experimental; urgency=low

  * New upstream release.
  * debian/control.in, debian/rules:
    + updated for the Gnome Team.

 -- Sebastien Bacher <seb128@debian.org>  Sun, 28 Mar 2004 22:32:50 +0200

gnome-system-monitor (2.4.0-1) unstable; urgency=low

  * New maintainer.
  * New upstream release.
  * Fixed Depends on libgtop2 (Closes: #210801).
  * Updated to Standards-Version 3.6.1.0 (no changes).
  * Updated patch.

 -- Sebastien Bacher <seb128@debian.org>  Tue, 23 Sep 2003 23:50:47 +0200

gnome-system-monitor (2.3.0-3) unstable; urgency=low

  * debian/control:
    - Build-Depend on the latest CDBS; this will pick up
      fixes to docbookxml.mk to fix the Docbook files
      (Closes: #195631)

 -- Colin Walters <walters@debian.org>  Fri,  6 Jun 2003 23:32:47 -0400

gnome-system-monitor (2.3.0-2) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version: 3.5.10, no changes required.
    - Build-Depend on cdbs.
  * debian/rules:
    - Convert to cdbs.
  * debian/rocks:
    - Removed.

 -- Colin Walters <walters@debian.org>  Sun, 25 May 2003 03:27:06 -0400

gnome-system-monitor (2.3.0-1) unstable; urgency=low

  * New upstream release.
    - Displays free space on media (Closes: #176243)
  * debian/control:
    - Bump versions on Build-Depends.
    - Don't conflict with gtop (Closes: #182226).
  * debian/rules:
    - Update to the latest version of Colin's Build System.

 -- Colin Walters <walters@debian.org>  Wed, 26 Mar 2003 00:52:12 -0500

gnome-system-monitor (2.0.4-1) unstable; urgency=low

  * New upstream release.
    - This will force a rebuild with the latest libc6 (Closes: #179950).
  * Remove cruft from .diff.gz.
  * debian/rules:
    - Update to the latest version of Colin's Build System.

 -- Colin Walters <walters@debian.org>  Thu,  6 Feb 2003 12:08:19 -0500

gnome-system-monitor (2.0.3-2) unstable; urgency=low

  * debian/rules:
    - Update to the latest version of Colin's Build System.
  * debian/rocks:
    - Fix up XML references (Closes: #171963).
    - Remove extra cruft in deb-extra-clean rule.

 -- Colin Walters <walters@debian.org>  Wed, 11 Dec 2002 01:31:40 -0500

gnome-system-monitor (2.0.3-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - Use Colin's Build System.
  * debian/control:
    - Build-Depend on the debhelper (>= 4.0.0), and remove dbs.
    - Build-Depend on gettext (Closes: #169502).
    - Bump Standards-Version to 3.5.8.
    - Depend on scrollkeeper.

 -- Colin Walters <walters@debian.org>  Wed, 27 Nov 2002 12:00:35 -0500

gnome-system-monitor (2.0.2-2) unstable; urgency=low

  * Don't be a Debian-native pacakge.  Sigh.
  * debian/control:
    - Standards-Version: 3.5.7.
  * debian/rules:
    - Support DEB_BUILD_OPTIONS=noopt
    - Remove useless test -r calls as part of config.{sub,guess} update.

 -- Colin Walters <walters@debian.org>  Sun, 15 Sep 2002 20:48:15 -0400

gnome-system-monitor (2.0.2-1) unstable; urgency=low

  * New upstream release.

 -- Colin Walters <walters@debian.org>  Thu, 29 Aug 2002 14:45:46 -0400

gnome-system-monitor (2.0.1-1.1) unstable; urgency=low

  * NMU
  * debian/control update build-dependency to latest libgnomeui-dev 2.0.3
    and rebuild against this library.


 -- Christian Marillat <marillat@debian.org>  Thu, 15 Aug 2002 10:09:18 +0200

gnome-system-monitor (2.0.1-1) unstable; urgency=low

  * New upstream version.
  * debian/control:
    - [gnome-system-monitor]: Conflict with gtop, too  (Closes: #153786)

 -- Colin Walters <walters@debian.org>  Mon, 29 Jul 2002 13:53:54 -0400

gnome-system-monitor (2.0.0.1-3) unstable; urgency=medium

  * debian/control:
    - Tighten up versions in Build-Depends.

 -- Colin Walters <walters@debian.org>  Fri, 19 Jul 2002 13:17:33 -0400

gnome-system-monitor (2.0.0.1-2) unstable; urgency=medium

  * debian/patches/69_docbook_reference.patch:
    - New patch; modify the Docbook external reference to be local
      (Closes: #153216)

 -- Colin Walters <walters@debian.org>  Wed, 17 Jul 2002 23:13:52 -0400

gnome-system-monitor (2.0.0.1-1) unstable; urgency=medium

  * Fake new upstream version so we can switch to a non-native package.
  * debian/control:
    - Add Build-Depends on scrollkeeper (Closes: #150852).

 -- Colin Walters <walters@debian.org>  Wed, 19 Jun 2002 22:02:19 -0400

gnome-system-monitor (2.0.0-1) unstable; urgency=medium

  * New upstream version.
  * Make description synopsis conform with the One True Way.
    (Closes: #149864)

 -- Colin Walters <walters@debian.org>  Wed, 12 Jun 2002 12:41:35 -0400

gnome-system-monitor (1.1.7-1) unstable; urgency=low

  * Initial Release (Closes: #148246).

 -- Colin Walters <walters@debian.org>  Sun, 26 May 2002 22:01:56 -0400
